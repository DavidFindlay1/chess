# Chess Scala Challenge - scalac 

## Problem
The problem is to find all unique configurations of a set of normal chess pieces on a chess board with
dimensions M×N where none of the pieces is in a position to take any of the others. Providing the number of
results is useful, but not enough to complete the assignment. Assume the colour of the piece does not
matter, and that there are no pawns among the pieces.

Write a program which takes as input:

* The dimensions of the board: M, N
* The number of pieces of each type (King, Queen, Bishop, Rook and Knight) to try and place on the
board.

As output, the program should list all the unique configurations to the console for which all of the pieces can
be placed on the board without threatening each other.
When returning your solution, please provide with your answer the total number of unique configurations for
a **7×7 board with 2 Kings, 2 Queens, 2 Bishops and 1 Knight. Also provide the time it took to get
the final score. Needless to say, the lower the time, the better.**

## Solution

The final solutions uses backtracking recursion dynamic programming to allow for early stopping if an invalid solution is 
detected. Tail recursion is used so that the scala compiler does not provision additional stack space.

The solution finds 3063828 board configurations in roughly 135 seconds. 

## Instructions

Different configurations can be run by changing the resources/application.conf file, including the flag which will be used 
to display the solution configurations, in this case set boardDisplay: true.  

To run: 

* git clone https://DavidFindlay1@bitbucket.org/DavidFindlay1/chess.git
* cd chess
* sbt compile
* sbt test
* sbt run
