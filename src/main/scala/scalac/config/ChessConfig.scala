package scalac.config

import com.typesafe.config.Config

case class ChessConfig(M: Int,
                       N: Int,
                       kings: Int,
                       queens: Int,
                       rooks:Int,
                       knights: Int,
                       bishops: Int,
                       boardDisplay:Boolean)

object ChessConfig{

  def apply(config: Config): ChessConfig = {

    val M = config.getInt("M")
    val N = config.getInt("N")
    val kings = config.getInt("kings")
    val queens = config.getInt("queens")
    val rooks = config.getInt("rooks")
    val knights = config.getInt("knights")
    val bishops = config.getInt("bishops")
    val boardDisplay = config.getBoolean("boardDisplay")

    ChessConfig(M, N, kings, queens, rooks, knights, bishops, boardDisplay)

  }
}
