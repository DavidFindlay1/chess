package scalac.utils

object MeasurementUtils {
  /** *
    *
    * @param f a function or code block to profile
    * @return prints the elapsed wall time in milliseconds and the memory used in MB.
    */
  def profiler[R](f: => R): R = {
    import System.{currentTimeMillis => time}
    val start = time
    val result = f
    val finish = time
    println(s"Elapsed time: ${finish - start} ms")
    println(s"Memory: ${Runtime.getRuntime.totalMemory>>20} MB")
    result
  }
}
