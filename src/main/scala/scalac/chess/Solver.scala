package scalac.chess

import scala.annotation.tailrec

object Solver {

  def getSolution(board: Board, pieces: List[Piece]): Set[Board] = {
    backtrackRecursion(pieces, Set(board)).filter(_.isComplete)
  }

  @tailrec
  private def backtrackRecursion(pieces: List[Piece], solutions: Set[Board]): Set[Board] = pieces match {
    case Nil => solutions
    case candidatePs :: ps => backtrackRecursion(
      ps,
      solutions.flatMap(board =>
        (for {
          r <- 1 to board.M
          c <- 1 to board.N
          p = PieceFactory.constructPiece(candidatePs, r, c)
          b = board.drop(p)
          if board.isSafe(p)
        } yield b).toSet))
  }
}