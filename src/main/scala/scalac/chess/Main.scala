package scalac.chess

import com.typesafe.config.{Config, ConfigFactory}
import scalac.utils.MeasurementUtils.profiler
import scalac.config.ChessConfig

object Main extends App {

  val chessConfig: Config = ConfigFactory.load().getConfig("chess-inputs")
  val conf = ChessConfig(chessConfig)

  val pieces = PieceFactory.constructPieceList(
    conf.kings,
    conf.queens,
    conf.bishops,
    conf.rooks,
    conf.knights)

  val solutions: Set[Board] = profiler {
    Solver.getSolution(Board(conf.M, conf.N, Set(), pieces.size), pieces)
  }

  println(s"Solutions: ${solutions.size}")

  // optionally display the configurations.
  if (conf.boardDisplay) solutions.foreach(b => println(b.format()))

}


