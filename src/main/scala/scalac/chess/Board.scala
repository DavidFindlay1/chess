package scalac.chess

case class Board(M: Int, N: Int, usedPieces: Set[Piece], numPieces: Int) {

  // Used to count the complete solutions
  def isComplete: Boolean = numPieces == usedPieces.size

  /** *
    *
    * @param p Piece drop onto a board
    * @return A new Board (immutable) with the piece added
    */
  def drop(p: Piece): Board = Board(M, N, usedPieces + p, numPieces)

  /** *
    *
    * @param p Piece to be dropped
    * @return true if p is not in the kill zone of the other pieces currently on the board
    */
  def isSafe(p: Piece): Boolean = {
    usedPieces.forall(currentPieces =>
      !p.attacks(currentPieces) && !currentPieces.attacks(p))
  }

  /** *
    *
    * Format the board with pieces on it for printing.
    */

  def format(): String = {

    val rows = (for {
      x <- 1 to M
      y <- 1 to N
    } yield usedPieces
      .find(p => p.row == x && p.col == y)
      .map(_.toString())
      .getOrElse(" "))
      .grouped(N)
      .toList

    rows
      .flatMap{ row => row.mkString("|", "|", "|") ++ "\n" }
      .mkString
  }

}
