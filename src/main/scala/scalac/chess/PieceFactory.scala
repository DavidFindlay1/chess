package scalac.chess

object PieceFactory {

  /** *
    *
    * @param p   Piece to create
    * @param row row location
    * @param col column location
    * @return piece dropped at row and column location
    */
  def constructPiece(p: Piece, row: Int, col: Int): Piece = p match {
    case _: King => King(row, col)
    case _: Queen => Queen(row, col)
    case _: Knight => Knight(row, col)
    case _: Rook => Rook(row, col)
    case _: Bishop => Bishop(row, col)
    case _ => throw new NotImplementedError(s"$p not implemented")
  }

  /** *
    *
    * @return List of pieces given
    */
  def constructPieceList(numKings: Int,
                         numQueens: Int,
                         numBishops: Int,
                         numRooks: Int,
                         numKnights: Int): List[Piece] = {

    (1 to numKings).map(_ => King(0,0)).toList ++
      (1 to numQueens).map(_ => Queen(0,0)).toList ++
        (1 to numBishops).map(_ => Bishop(0,0)).toList ++
          (1 to numRooks).map(_ => Rook(0,0)).toList ++
            (1 to numKnights).map(_ => Knight(0,0))
  }
}