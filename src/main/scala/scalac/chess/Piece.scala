package scalac.chess

sealed trait Piece {
  def row: Int

  def col: Int

  def attacks(other: Piece): Boolean
}

case class King(row: Int, col: Int) extends Piece {
  private val directions = List(-1, 1, 0)

  private val moves: Seq[(Int, Int)] = {
    for (horizontal <- directions;
         vertical <- directions)
      yield (row + horizontal, col + vertical)
  }

  override def attacks(other: Piece): Boolean = {
    moves.contains((other.row, other.col))
  }

  override def toString = "K"
}

case class Queen(row: Int, col: Int) extends Piece {
  override def attacks(other: Piece): Boolean =
    other.row == row ||
      other.col == col ||
      Math.abs(other.row - row) == Math.abs(other.col - col)

  override def toString = "Q"

}

case class Rook(row: Int, col: Int) extends Piece {
  override def attacks(other: Piece): Boolean = other.row == row || other.col == col

  override def toString = "R"
}

case class Bishop(row: Int, col: Int) extends Piece {
  override def attacks(other: Piece): Boolean = Math.abs(other.row - row) == Math.abs(other.col - col)

  override def toString = "B"
}

case class Knight(row: Int, col: Int) extends Piece {
  private val directions = List(-2, -1, 1, 2)

  private val moves: Seq[(Int, Int)] = {
    for (horizontal <- directions;
         vertical <- directions if Math.abs(horizontal) != Math.abs(vertical))
      yield (row + horizontal, col + vertical)
  }

  override def attacks(other: Piece): Boolean = moves.contains((other.row, other.col))

  override def toString = "N"
}