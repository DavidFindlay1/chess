import org.scalatest.FunSuite

import scalac.chess._

// Write integration testing for the cases provided in the problem description.

class ChessTest extends FunSuite {
  test("3x3 board containing 2 Kings and 1 Rook") {
    val pieces = PieceFactory.constructPieceList(2,0,0,1,0)
    val solutions = Solver.getSolution(Board(3, 3, Set(), pieces.length), pieces)
    assert(solutions.size == 4)
  }
  test("4x4 board containing 2 Rooks and 4 Knights") {
    val pieces = PieceFactory.constructPieceList(0,0,0,2,4)
    val solutions = Solver.getSolution(Board(4, 4, Set(), pieces.length), pieces)
    assert(solutions.size == 8)
  }
}
