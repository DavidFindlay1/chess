import org.scalatest.FunSuite

import scalac.chess.{King, Queen, Rook, Knight, Bishop}

// Test out the pieces attacking the others.

class PieceTest extends FunSuite {
  test("test King attacks") {
    assert(King(3, 3).attacks(King(4, 2)))
    assert(King(3, 3).attacks(King(3, 2)))
    assert(King(3, 3).attacks(King(2, 2)))
    assert(King(3, 3).attacks(King(2, 3)))
    assert(King(3, 3).attacks(King(2, 4)))
    assert(King(3, 3).attacks(King(3, 4)))
    assert(King(3, 3).attacks(King(4, 4)))
    assert(!King(3, 3).attacks(King(8, 10)))
  }
  test("test Queen attacks") {
    assert(Queen(3, 3).attacks(Queen(5, 5)))
    assert(Queen(3, 3).attacks(Queen(4, 4)))
    assert(Queen(3, 3).attacks(Queen(2, 4)))
    assert(Queen(3, 3).attacks(Queen(1, 5)))
    assert(Queen(3, 3).attacks(Queen(5, 1)))
    assert(Queen(3, 3).attacks(Queen(4, 2)))
    assert(Queen(3, 3).attacks(Queen(2, 2)))
    assert(Queen(3, 3).attacks(Queen(1, 1)))
    assert(Queen(3, 3).attacks(Queen(4, 3)))
    assert(Queen(3, 3).attacks(Queen(5, 3)))
    assert(Queen(3, 3).attacks(Queen(2, 3)))
    assert(Queen(3, 3).attacks(Queen(1, 3)))
    assert(Queen(3, 3).attacks(Queen(3, 4)))
    assert(Queen(3, 3).attacks(Queen(3, 5)))
    assert(Queen(3, 3).attacks(Queen(3, 2)))
    assert(Queen(3, 3).attacks(Queen(3, 1)))
    assert(!Queen(2, 2).attacks(Queen(4, 5)))
  }
  test("test Bishop attacks") {
    assert(Bishop(3, 3).attacks(Bishop(5, 5)))
    assert(Bishop(3, 3).attacks(Bishop(4, 4)))
    assert(Bishop(3, 3).attacks(Bishop(2, 4)))
    assert(Bishop(3, 3).attacks(Bishop(1, 5)))
    assert(Bishop(3, 3).attacks(Bishop(5, 1)))
    assert(Bishop(3, 3).attacks(Bishop(4, 2)))
    assert(Bishop(3, 3).attacks(Bishop(2, 2)))
    assert(Bishop(3, 3).attacks(Bishop(1, 1)))
  }
  test("test Rook attacks") {
    assert(Rook(3, 3).attacks(Rook(4, 3)))
    assert(Rook(3, 3).attacks(Rook(5, 3)))
    assert(Rook(3, 3).attacks(Rook(2, 3)))
    assert(Rook(3, 3).attacks(Rook(1, 3)))
    assert(Rook(3, 3).attacks(Rook(3, 4)))
    assert(Rook(3, 3).attacks(Rook(3, 5)))
    assert(Rook(3, 3).attacks(Rook(3, 2)))
    assert(Rook(3, 3).attacks(Rook(3, 1)))
    assert(!Rook(3, 3).attacks(Rook(5, 0)))
  }
  test("test Knight attacks") {
    assert(Knight(3, 3).attacks(Knight(5, 4)))
    assert(Knight(3, 3).attacks(Knight(4, 5)))
    assert(Knight(3, 3).attacks(Knight(2, 5)))
    assert(Knight(3, 3).attacks(Knight(1, 4)))
    assert(Knight(3, 3).attacks(Knight(1, 2)))
    assert(Knight(3, 3).attacks(Knight(1, 2)))
    assert(Knight(3, 3).attacks(Knight(2, 1)))
    assert(Knight(3, 3).attacks(Knight(4, 1)))
  }
}