name := "Chess"

version := "1.0"

scalaVersion := "2.12.3"

connectInput in run := true

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.4" % "test",
  "com.typesafe" % "config" % "1.3.1")

javaOptions += "-XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -Xmx4g"
